{$DEFINE BASICOFF}

uses
  Graph, Crt, SysUtils, unicodeRenderer;

var
  fileNames: array[0..50] of string[13];
  fileCount: Byte;


procedure FindFiles;
var
  info: TSearchRec;
begin
  fileCount := 0;
  if FindFirst('D:*.TXT', faAnyFile, info) = 0 then
  begin
    repeat
      fileNames[fileCount] := Copy(info.Name, 1, Length(info.Name));
      Inc(fileCount);
    until FindNext(info) <> 0;
    FindClose(info);
  end;
end;


procedure ProcessFile(i: Byte);
var
  buffer: array[0..1000] of Byte;
  f: File;
  len: Word;
  fileName: TString;
  tmp: string;
begin
  tmp := fileNames[i];
  fileName := 'D:            ';
  SetLength(fileName, Length(tmp) + 2);
  Move(Pointer(@tmp[1]), Pointer(@fileName[3]), Length(tmp));

  Assign(f, fileName);
  Reset(f, 1);
  BlockRead(f, buffer, SizeOf(buffer), len);
  Close(f);
      
  buffer[len] := 0;
  RenderUtf8Text(buffer);

  Delay(1000);
  NextRow;
  NextRow;
end;


var
  SAVMSC: Word absolute $58;
  COLOR1: Byte absolute $02C5;
  COLOR2: Byte absolute $02C6;
  i: Byte;
begin
  InitGraph(8+16);
  COLOR1 := $0C;
  COLOR2 := $02;
  Init(SAVMSC);
  autoScroll := True;

  FindFiles;

  i := 0;
  repeat
    ProcessFile(i);
    Inc(i);
    if i = fileCount then
    begin
      i := 0;
    end;
  until KeyPressed;

  InitGraph(0);
end.