unit unicodeRenderer;

interface

var
  autoScroll: Boolean;

procedure Init(displayAddr: Word);
procedure SetPosition(column, row: Byte);
procedure RenderUtf8Text(ptr: PByte);
procedure NextRow;

implementation

uses
  unicodeFont;

const
  CHAR_HEIGHT = 16;
  COLUMN_COUNT = 40;
  ROW_COUNT = 12;
  LINE_COUNT = ROW_COUNT * CHAR_HEIGHT;

var
  displayRow0Addr, displayRow1Addr, displayRowLastAddr: Word;
  lineAddrLo: array[0..LINE_COUNT - 1] of Byte;
  lineAddrHi: array[0..LINE_COUNT - 1] of Byte;
  currRow: Byte;
  currRowAddrLo: array[0..CHAR_HEIGHT - 1] of Byte;
  currRowAddrHi: array[0..CHAR_HEIGHT - 1] of Byte;
  zpCurrCharAddr: Word absolute $E0;
  zpCurrDispAddr: Word absolute $E2;
  currColumn: Byte absolute $E4;


procedure UpdatePosition; assembler;
asm
{
    lda currRow
    :4 asl
    add #CHAR_HEIGHT-1
    tax
    ldy #CHAR_HEIGHT-1
loop
    lda adr.lineAddrLo,x
    sta adr.currRowAddrLo,y
    lda adr.lineAddrHi,x
    sta adr.currRowAddrHi,y
    dex
    dey
    bpl loop
};
end;


procedure SetPosition(column, row: Byte);
var
  newRow: Boolean;
begin
  if column < COLUMN_COUNT then
  begin
    currColumn := column;
  end
  else begin
    currColumn := COLUMN_COUNT - 1;
  end;

  if row >= ROW_COUNT then
  begin
    row := ROW_COUNT - 1;
  end;

  if currRow <> row then
  begin
    currRow := row;
    UpdatePosition;
  end;
end;


procedure ScrollScreenOneRow;
const
  ROW_SIZE: Word = CHAR_HEIGHT * COLUMN_COUNT;
  SIZE: Word = (ROW_COUNT - 1) * ROW_SIZE;
begin
  Move(Pointer(displayRow1Addr), Pointer(displayRow0Addr), SIZE);
  FillChar(Pointer(displayRowLastAddr), ROW_SIZE, 0);
end;


procedure NextRow;
begin
  currColumn := 0;
  Inc(currRow);
  if currRow >= ROW_COUNT then
  begin
    if autoScroll then
    begin
      ScrollScreenOneRow;
      Dec(currRow);
    end
    else begin
      currRow := 0;
      UpdatePosition;
    end;
  end
  else begin
    UpdatePosition;
  end;
end;


procedure NextPosition; keep;
begin
  Inc(currColumn);
  if currColumn >= COLUMN_COUNT then
  begin
    NextRow;
  end;
end;


procedure Init(displayAddr: Word);
var
  i: Byte;
begin
  displayRow0Addr := displayAddr;
  displayRow1Addr := displayAddr + CHAR_HEIGHT * COLUMN_COUNT;
  displayRowLastAddr := displayAddr + CHAR_HEIGHT * COLUMN_COUNT * (ROW_COUNT - 1);
  for i := 0 to LINE_COUNT - 1 do
  begin
    lineAddrLo[i] := Lo(displayAddr);
    lineAddrHi[i] := Hi(displayAddr);
    Inc(displayAddr, COLUMN_COUNT);
  end;

  currColumn := 0;
  currRow := 0;
  UpdatePosition;
end;


procedure RenderChar; assembler;
asm
{
    ldx #CHAR_HEIGHT-1
loop
    lda adr.currRowAddrLo,x
    sta zpCurrDispAddr
    lda adr.currRowAddrHi,x
    sta zpCurrDispAddr+1

    txa
    tay
    lda (zpCurrCharAddr),y
    ldy currColumn
    sta (zpCurrDispAddr),y
    dex
    bpl loop

    jsr NextPosition
};
end;


procedure RenderUtf8Text(ptr: PByte);
var
  code: Word;
begin
  while ptr^ <> 0 do
  begin
    if ptr^ < $80 then
    begin
      code := ptr^;
      Inc(ptr);
    end
    else if ptr^ and $E0 = $C0 then
    begin
      code := ptr^ and $1F;
      code := code shl 6;
      Inc(ptr);
      code := code or (ptr^ and $3F);
      Inc(ptr);
    end
    else if ptr^ and $F0 = $E0 then
    begin
      code := ptr^ and $0F;
      code := code shl 6;
      Inc(ptr);
      code := code or (ptr^ and $3F);
      code := code shl 6;
      Inc(ptr);
      code := code or (ptr^ and $3F);
      Inc(ptr);
    end
    else begin
      code := $FFFF;
      Inc(ptr, 4);
    end;

    if code >= $20 then
    begin
      SetCurrCharAddress(code);
      RenderChar;
    end
    else if code = $0A then
    begin
      NextRow;
    end;
  end;
end;

end.