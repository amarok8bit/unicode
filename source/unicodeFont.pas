unit unicodeFont;

interface

const
  CHAR_HEIGHT = 16;
  FONT0_ADDR = $4000;
  FONT1_ADDR = $4800;
  FONT2_ADDR = $5000;
  FONT4_ADDR = $5800;
  FONT7_ADDR = $6000;
  FONT8_ADDR = $6800;
  FONT9_ADDR = $7000;
  FONT10_ADDR = $7800;
  FONT11_ADDR = $8000;
  FONT33_ADDR = $8800;
  
{$R resources.rc}

var
  zpCurrCharAddr: Word absolute $E0;

procedure SetCurrCharAddress(code: Word);

implementation

const
  DUMMY_CHAR: array[0..CHAR_HEIGHT - 1] of Byte = (
    %00000000,
    %01111110,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01000010,
    %01111110,
    %00000000);

const
  PAGE_COUNT = 10;
  PAGE_INDICES: array[0..PAGE_COUNT - 1] of Byte = (
    0, 1, 2, 4, 7, 8, 9, 10, 11, 33);
  PAGE_ADDRESSES: array[0..PAGE_COUNT - 1] of Word = (
    FONT0_ADDR, FONT1_ADDR, FONT2_ADDR, FONT4_ADDR, FONT7_ADDR,
    FONT8_ADDR, FONT9_ADDR, FONT10_ADDR, FONT11_ADDR, FONT33_ADDR);
  PAGE_NOT_FOUND = 255;

var
  lastPage: Byte;
  lastPageAdress: Word;

procedure SetCurrCharAddress(code: Word);
var
  i, page, ch: Byte;
begin
  page := Hi(code) shl 1;
  if Lo(code) and $80 <> 0 then
  begin
    Inc(page);
  end;

  if page = lastPage then
  begin
    if page = PAGE_NOT_FOUND then
    begin
      zpCurrCharAddr := Word(@DUMMY_CHAR[0])
    end
    else begin
      zpCurrCharAddr := lastPageAdress;
      ch := code and $7F;
      Inc(zpCurrCharAddr, 16 * ch);
    end;
  end
  else begin
    i := 0;
    repeat
      if PAGE_INDICES[i] = page then
      begin
        lastPage := page;
        lastPageAdress := PAGE_ADDRESSES[i];
        zpCurrCharAddr := lastPageAdress;
        ch := code and $7F;
        Inc(zpCurrCharAddr, 16 * ch);
        Exit;
      end;
      Inc(i);
    until i = PAGE_COUNT;
  
    zpCurrCharAddr := Word(@DUMMY_CHAR[0]);
    lastPage := PAGE_NOT_FOUND;
  end;
end;

initialization
begin
  lastPage := PAGE_NOT_FOUND;
end;

end.