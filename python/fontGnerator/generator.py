import cv2
import numpy as np

char_width = 8
char_height = 16
char_full_width = 16
map_offset_x = 32
map_offset_y = 64
subset_chars = 128
subset_size = subset_chars * char_height


def encode(line):
    result = 0
    for v in line:
        result *= 2
        if v == 0:
            result += 1
    return result


def extract_subset(index):
    subset = np.zeros(subset_size, dtype='uint8')

    first = index * subset_chars
    last = first + subset_chars
    index = 0
    for char in range(first, last):
        src_x = map_offset_x + (char % 256) * char_full_width
        src_y = map_offset_y + (char // 256) * char_height
        for y in range(char_height):
            subset[index] = encode(full_map[src_y + y, src_x:src_x+char_width, 0])
            index += 1

    return subset


full_map = cv2.imread('unifont-15.0.01.bmp')
subsets = [0, 1, 2, 4, 7, 8, 9, 10, 11, 33]
for s in subsets:
    data = extract_subset(s)
    data.tofile(f'font{s}.dat')
